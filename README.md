# Nim 1.0.2 on OpenBSD 6.6

from the install [doc](https://nim-lang.org/install_unix.html):

~~~~~
There are a number of other dependencies that you may need to install in order to use Nim. They include:

PCRE
OpenSSL
~~~~~

on OpenBSD 6.6 command line:

~~~~
$ pkg_info | grep pcre   
pcre-8.41p2         perl-compatible regular expression library
pcre2-10.33         perl-compatible regular expression library, version 2
$ pkg_info | grep gcc 
gcc-8.3.0p4         GNU compiler collection: core C compiler
gcc-libs-8.3.0p4    GNU compiler collection: support libs
$ pkg_info | grep wget
wget-1.20.3p1       retrieve files from the web via HTTP, HTTPS and FTP
$ pkg_info | grep xz
xz-5.2.4            LZMA compression and decompression tools
~~~~

download the source, check sha256 against [hash](https://nim-lang.org/download/nim-1.0.2.tar.xz.sha256) and install:
~~~~
$ wget https://nim-lang.org/download/nim-1.0.2.tar.xz
$ sha256 nim-1.0.2.tar.xz
$ xzcat ./nim-1.0.2.tar.xz | tar -xvf -
$ cd nim-1.0.2
$ export CC=egcc
$ ./build.sh
$ doas ./install.sh /usr/local/bin
$ /usr/local/bin/nim c koch
$ ./koch test cat concepts 
$ ./koch test cat modules
~~~~
